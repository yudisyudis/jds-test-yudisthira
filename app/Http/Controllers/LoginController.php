<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Firebase\JWT\JWT;




class LoginController extends Controller
{
    public function index()
    {
        return view('login');
    }
//     public function update(Request $request, $id)
//     {
//         $student = Student::find($id);
//         $student->name = $request->input('name');
//         $student->email = $request->input('email');
//         $student->course = $request->input('course');
//         $student->section = $request->input('section');
//         $student->update();
//         return redirect()->back()->with('status','Student Updated Successfully');
//     }
// }
    

    public function store()
    {
        $token = request()->_token;
        $user = User::where('username', request()->username)->first();
        $user->_token = $token;
        $user->save();

    
        return view('data', [
            'users' => User::where('username', request()->username)->get()
        ]);

        // $user = User::where('username', $username);
        // $user->username = $request->username;
        // $user->password = $request->password;
        // $user->token = $request->_token;
        // $user->update();

        // $user = new User;
        // $user->id = User::where('username', request()->username)->pluck('id')->first();
        // $user->username = request()->username;
        // $user->token = request()->_token;

      
       
    }
           

    public function json()
    {
        $user = User::orderBy('updated_at', 'desc')->first();
        return $user;
       
        
        // $user = new User;
        // $user->id = User::where('username', request()->username)->pluck('id')->first();
        // $user->username = request()->username;
        // $user->token = request()->_token;

        // return view('data_json', [
        //     'users' => User::where('username', request()->username)->first()
        // ]);
        // return view('data_json', [
        //     'users' => User::latest()->pluck('id', 'username', 'token')->get()
        // ]);
       
    }

    public function validasi(Request $request)
    {
        $token = User::where('username', $request->username)->pluck('_token')->first();

        $token_time = date("d-m-y");
        $expired_at = date('d-m-y', strtotime($token_time. ' + 1 days'));

        if ($token_time > $expired_at) {
            request()->session()->invalidate();
            request()->session()->regenerateToken();
            echo "Token Expired";
        }
       
        
        if ($request->_token == $token ) {
            $user = new User;
            $user->username =  $request->username;
            $user->is_valid = True;
            $user->expired_at = $expired_at;
        }else{
            $user = new User;
            $user->is_valid = False;
        }
        return $user;
    }

    public function logout()
    {
        request()->session()->invalidate();
     
        request()->session()->regenerateToken();
     
        return redirect('/');
    }

}
