<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    public function index()
    {
        return view('register');
    }

    public function store(Request $request)
    {
        $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ1234567890');
        $password = substr($random, 0, 6);

        $request->validate([
            'username' => 'required',
        ]);

        User::create([
            'username' => $request['username'],
            'role' => $request['flexRadioDefault'],
            'password' => $password,
            'token' => $request['_token']
        ]);
        

        return view('info', [
            'users' => User::latest()->get()
        ]);

    }

}
