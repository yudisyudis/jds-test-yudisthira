@extends('main')

@section('container')
@foreach ($users->take(1) as $user)
<h3 class="text-center p-2" style="background-color: rgba(0, 0, 0, 0.7); color:white; border-radius: 20px;">Selamat Datang {{ $user->username }}</h3>
@endforeach

<h4 class="text-center p-2" style="background-color: rgba(255, 255, 255, 0.9); border-radius: 20px;">Berikut informasi data anda</h2>
<div class="card text-center mb-5" style="border-radius: 20px; background-color: rgba(255, 255, 255, 0.95);">
    <div class="card-header">
      <ul class="nav nav-tabs card-header-tabs">
        <li class="nav-item">
          <a class="nav-link active" aria-current="true" href="/login">Form</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/json_log">JSON</a>
        </li>
      </ul>
    </div>
    <div class="card-body" >
        <div class="row justify-content-center m-auto">
            <div class="col-md-5 justify-content-center">
               
                <label for="" style="margin-top: 10px">ID</label>
                <div class="card">
                    @foreach ($users->take(1) as $user)
                        <div class="card-body md-4">{{ $user->id }}</div>
                    @endforeach
                </div>
                <label for="" style="margin-top: 10px">Username</label>
                <div class="card">
                    @foreach ($users->take(1) as $user)
                        <div class="card-body md-4">{{ $user->username }}</div>
                    @endforeach
                </div>
                <label for="" style="margin-top: 10px">Token</label>
                <div class="card">
                    @foreach ($users->take(1) as $user)
                        <div class="card-body md-4">{{ $user->_token }}</div>
                    @endforeach
                </div>    
            </div>
        </div>
        <form method="post" action="/validasi">
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />

            @foreach ($users->take(1) as $user)
                <input type="hidden" name="username" value="{{ $user->username }}" />
            @endforeach
			<input type="submit" name="submit" value="Validasi Token" class="btn btn-primary my-4">
		</form>

        <form action="/logout" method="post" class="mb-3">
            @csrf
            <button type="submit" class="dropdown-item nav-link text-dark px-3 border-0">Logout<span data-feather="log-out"></span></button>
        </form>
        {{-- @php
            if (isset($_POST['submit'])) {
                if ($user->token == $_POST['csrf']) {
                    echo "CSRF Token Success";
                } else
                    echo 'CSRF Token Failed!';
            }
        @endphp --}}
    </div>
</div>

@endsection

