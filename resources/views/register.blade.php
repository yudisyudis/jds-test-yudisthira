@extends('main')

@section('container')

<div class="row justify-content-center" >
    <div class="col-lg-5">
        <main class="form-regis w-100 m-auto card p-5" style="background-color: rgba(255, 255, 255, 0.9); ">
            <h1 class="h3 mb-3 fw-normal text-center">Form Registrasi</h1>
            <form action="/" method="post">
                @csrf
          
                <div class="form-floating">
                    <input type="text" name="username" class="form-control @error('username') is-invalid @enderror" id="username" placeholder="Username" required value="{{ old('username') }}">
                    <label for="username">Username</label>
                    @error('username')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="my-3">
                    <label for=""><strong>Role:</strong></label>
                    
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="super_admin" value="Super Admin">
                        <label class="form-check-label" for="super_admin">
                          Super Admin
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="admin" value="Admin">
                        <label class="form-check-label" for="admin">
                          Admin
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="user" value="User" checked>
                        <label class="form-check-label" for="user">
                          User
                        </label>
                    </div>
                </div>
                

                {{-- <div class="form-floating">
                    <input type="password" id="password" name="password" class="form-control form-control rounded-bottom @error('password') is-invalid @enderror onclick="genPassword()" id="password" placeholder="Password" required >
                    <label for="password">Password</label>
                    @error('password')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div> --}}
                
                <button class="w-100 btn btn-lg btn-primary mt-4" type="submit">Register</button>
            </form>
            <small class="d-block text-center mt-3">Sudah Register? <a href="/login">Silahkan login</a></small>


        </main>
    </div>
</div>



@endsection