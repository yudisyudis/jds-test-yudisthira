@extends('main')

@section('container')
<div class="row justify-content-center m-auto" >
    <div class="col-md-5 justify-content-center card p-5" style="background-color: rgba(255, 255, 255, 0.9); border-radius: 20px;">
        <h2 class="text-center">Informasi User</h2>
        <label for="" style="margin-top: 10px">Username</label>
        <div class="card">
            @foreach ($users->take(1) as $user)
                <div class="card-body md-4">{{ $user->username }}</div>
            @endforeach
        </div>
        <label for="" style="margin-top: 10px">Role</label>
        <div class="card">
            @foreach ($users->take(1) as $user)
                <div class="card-body md-4">{{ $user->role }}</div>
            @endforeach
        </div>
        <label for="" style="margin-top: 10px">Password</label>
        <div class="card">
            @foreach ($users->take(1) as $user)
                <div class="card-body md-4">{{ $user->password }}</div>
            @endforeach
        </div>
        <small class="d-block text-center mt-3 bg-light">Silahkan <a href="/login">Login</a></small>
    </div>
</div>

@endsection

