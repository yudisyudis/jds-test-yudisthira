@extends('main')

@section('container')
<div class="row justify-content-center">
    <div class="col-lg-4">

      @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          {{ session('success') }}
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
      @endif

      @if (session()->has('loginError'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          {{ session('loginError') }}
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
      @endif

        <main class="form-signin w-100 m-auto card p-5" style="background-color: rgba(255, 255, 255, 0.9); ">
            <h1 class="h3 mb-3 fw-normal text-center">Please Login</h1>
            <form action="/login" method="post">
              @csrf
          
              <div class="form-floating">
                <input type="text" name="username" class="form-control"   id="username" placeholder="username" autofocus required value="{{ old('username') }}">
                <label for="username">Username</label>
              </div>

              <div class="form-floating">
                <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                <label for="password">Password</label>
              </div>
              
              <button class="w-100 btn btn-lg btn-primary" type="submit">Login</button>
            </form>

            <small class="d-block text-center mt-3">Belum Register? <a href="/">Silahkan registrasi</a></small>
        </main>
    </div>
</div>
@endsection