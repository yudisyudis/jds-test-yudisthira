@extends('main')

@section('container')

@php
    if (isset($_POST['submit'])) {
		if (hash_equals($csrf, $_POST['csrf'])) {
			echo "Your name is: " . $_POST['username'];
		} else
			echo 'CSRF Token Failed!';
	}
@endphp

@endsection

